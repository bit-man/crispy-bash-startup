package guru.bitman.bashstartup;

import guru.bitman.bashstartup.config.BashStartupConfig;
import guru.bitman.bashstartup.system.SystemFactory;
import guru.bitman.fictionalvieira.command.CommandFactory;

import java.io.*;

public class Startup
{
    private static final String configPath = ".bash-startup" + File.separator + "config.yaml";
    private static File configFile = new File(System.getProperty("user.home"), configPath);

    public static void init()
            throws FileNotFoundException
    {
        CommandFactory.reset();
        BashStartupConfig config = BashStartupConfig.load(new FileInputStream(configFile));
        CommandFactory.addCommand("startupApps",  cmdLine -> new StartupAppsCommand(config, SystemFactory.getInstance()));
    }
}
