package guru.bitman.bashstartup;

import guru.bitman.bashstartup.config.BashStartupConfig;
import guru.bitman.bashstartup.config.ConfigProperties;
import guru.bitman.bashstartup.system.SystemAbstractionLayer;
import guru.bitman.fictionalvieira.bash.BashScript;
import guru.bitman.fictionalvieira.bash.GenericBashCommand;
import guru.bitman.fictionalvieira.server.Command;

import java.io.IOException;
import java.io.Writer;
import java.util.function.Consumer;


class StartupAppsCommand
        implements Command
{

    private final BashScript bashScript;
    private final BashStartupConfig config;
    private SystemAbstractionLayer system;

    StartupAppsCommand(BashStartupConfig config, SystemAbstractionLayer system)
    {
        this.config = config;
        this.system = system;
        bashScript = new BashScript();
    }

    @Override
    public void execute(Writer out)
    {
        Consumer<ConfigProperties> configPropertiesConsumer = property ->
        {
            if (system.processes().stream().noneMatch(proc -> proc.getCommand().matches(property.getLocationRegex())))
            {
                bashScript.addCommand(new GenericBashCommand(property.getCommand()));
            }
        };

        config.startupConfigIterator().forEachRemaining(
                configPropertiesConsumer
        );

        try
        {
            out.append(bashScript.buildScript());
            out.flush();
        } catch (IOException e)
        {
            e.printStackTrace(System.err);
        }
    }
}
