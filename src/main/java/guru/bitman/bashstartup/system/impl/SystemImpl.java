package guru.bitman.bashstartup.system.impl;

import guru.bitman.bashstartup.system.ProcessInfo;
import guru.bitman.bashstartup.system.SystemAbstractionLayer;
import org.jutils.jprocesses.JProcesses;

import java.util.ArrayList;
import java.util.Collection;

public class SystemImpl
    implements SystemAbstractionLayer
{
    private static SystemImpl singleton = new SystemImpl();

    private SystemImpl(){

    }

    @Override
    public Collection<ProcessInfo> processes()
    {
        Collection<ProcessInfo> processes = new ArrayList<>();

        JProcesses.getProcessList().stream()
                .forEach( p -> processes.add( new ProcessInfoAdapter(p) ) );

        return processes;
    }

    public static SystemAbstractionLayer getInstance()
    {
        return singleton;
    }
}
