package guru.bitman.bashstartup.system.impl;

import org.jutils.jprocesses.model.ProcessInfo;

public class ProcessInfoAdapter
        implements guru.bitman.bashstartup.system.ProcessInfo
{
    private final ProcessInfo p;

    public ProcessInfoAdapter(ProcessInfo p)
    {
        this.p = p;
    }

    @Override
    public String getCommand()
    {
        return p.getCommand();
    }
}
