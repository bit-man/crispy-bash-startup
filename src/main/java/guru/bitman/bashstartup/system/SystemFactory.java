package guru.bitman.bashstartup.system;

import guru.bitman.bashstartup.system.impl.SystemImpl;

public class SystemFactory
{
    public static SystemAbstractionLayer getInstance()
    {
        return SystemImpl.getInstance();
    }
}
