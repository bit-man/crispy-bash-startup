package guru.bitman.bashstartup.system;


import java.util.Collection;

public interface SystemAbstractionLayer
{
    Collection<ProcessInfo> processes();
}
