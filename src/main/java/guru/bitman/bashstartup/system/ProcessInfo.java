package guru.bitman.bashstartup.system;

public interface ProcessInfo
{
    String getCommand();
}
