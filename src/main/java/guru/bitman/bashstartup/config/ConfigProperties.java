package guru.bitman.bashstartup.config;

import java.util.Map;

public class ConfigProperties
{
    private Map<String, String> properties;

    ConfigProperties(Map<String, String> map)
    {
        this.properties = map;
    }

    public String getLocationRegex()
    {
        return properties.get("commandRegex");
    }

    public String getCommand()
    {
        return properties.get("command");
    }
}
