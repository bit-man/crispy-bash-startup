package guru.bitman.bashstartup.config;

import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class BashStartupConfig
{
    private List<Map<String, String>> startup;

    public static BashStartupConfig load(InputStream configAsStream) {
        Constructor constructor = new Constructor(BashStartupConfig.class);
        TypeDescription bashStartupConfigDescription = new TypeDescription(BashStartupConfig.class);
        bashStartupConfigDescription.putListPropertyType("startup", Map.class);
        constructor.addTypeDescription(bashStartupConfigDescription);
        Yaml yaml = new Yaml(constructor);
        return (BashStartupConfig) yaml.load(configAsStream);
    }

    public void setStartup(List<Map<String, String>> startupParams)
    {
        this.startup = startupParams;
    }

    public Iterator<ConfigProperties> startupConfigIterator() {
        return new StartupConfigIterator();
    }

    private class StartupConfigIterator
            implements Iterator<ConfigProperties>
    {


        private int next = 0;

        @Override
        public boolean hasNext()
        {
            return next < startup.size();
        }

        @Override
        public ConfigProperties next()
        {
            return new ConfigProperties( startup.get(next++) );
        }
    }
}
