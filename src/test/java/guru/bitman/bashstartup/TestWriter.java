package guru.bitman.bashstartup;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

class TestWriter
        extends Writer
{
    private List<String> buffer = new ArrayList<>();

    @Override
    public void write(char[] cbuf, int off, int len)
            throws IOException
    {
        buffer.add(new String(cbuf, off, len));
    }

    @Override
    public void flush()
            throws IOException
    {

    }

    @Override
    public void close()
            throws IOException
    {

    }

    int numLines()
    {
        return buffer.size();
    }

    String line(int i)
    {
        return buffer.get(i);
    }
}
