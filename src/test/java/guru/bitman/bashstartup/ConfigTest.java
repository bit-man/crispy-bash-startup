package guru.bitman.bashstartup;


import guru.bitman.bashstartup.config.BashStartupConfig;
import guru.bitman.bashstartup.config.ConfigProperties;
import org.junit.Before;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class ConfigTest
{
    private InputStream resourceAsStream;
    private List<Map<String, String>> startupList;

    @Before
    public void setUp()
    {
        this.resourceAsStream = getClass().getResourceAsStream("/test_config.yaml");
    }

    @Test
    public void testConfigContainsKeyForStartupComand()
    {
        @SuppressWarnings("unchecked")
        Map<String, Object> map = (Map<String, Object>) new Yaml().load(resourceAsStream);
        assertEquals(1, map.keySet().size());
        assertTrue(map.containsKey("startup"));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testStartupConfigContainsOneKey()
    {
        Map<String, Object> map = (Map<String, Object>) new Yaml().load(resourceAsStream);
        this.startupList = (List<Map<String, String>>) map.get("startup");
        assertEquals(2, startupList.size());

        assertMappingContents(0, "regex1", "command1 --param");
        assertMappingContents(1, "regex2", "command2");
    }

    @Test
    public void testConfigLoad()
    {
        BashStartupConfig config = BashStartupConfig.load(resourceAsStream);
        Iterator<ConfigProperties> configPropertiesIterator = config.startupConfigIterator();

        assertStartupConfigProperties(configPropertiesIterator, "regex1", "command1 --param");
        assertStartupConfigProperties(configPropertiesIterator, "regex2", "command2");

        assertFalse(configPropertiesIterator.hasNext());
    }

    private void assertStartupConfigProperties(Iterator<ConfigProperties> configPropertiesIterator, String regexp,
                                               String command)
    {
        assertTrue(configPropertiesIterator.hasNext());

        ConfigProperties property = configPropertiesIterator.next();
        assertEquals(regexp, property.getLocationRegex());
        assertEquals(command, property.getCommand());
    }


    private void assertMappingContents(int index, String regex, String command)
    {
        Map<String, String> firstMapping = startupList.get(index);
        assertTrue(firstMapping.containsKey("commandRegex"));
        assertEquals(regex, firstMapping.get("commandRegex"));
        assertTrue(firstMapping.containsKey("command"));
        assertEquals(command, firstMapping.get("command"));
    }
}
