package guru.bitman.bashstartup;

import guru.bitman.bashstartup.config.BashStartupConfig;
import guru.bitman.bashstartup.system.ProcessInfo;
import guru.bitman.bashstartup.system.SystemAbstractionLayer;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StartupAppsCommandTest
{
    private static BashStartupConfig config1app;
    private static BashStartupConfig config2apps;

    private SystemAbstractionLayer systemMock;
    private TestWriter out;

    @BeforeClass
    public static void beforeClass() {
        config1app = BashStartupConfig.load(StartupAppsCommandTest.class.getResourceAsStream("/startupappstest_1app_config.yaml"));
        config2apps = BashStartupConfig.load(StartupAppsCommandTest.class.getResourceAsStream("/startupappstest_2apps_config.yaml"));
    }

    @Before
    public void setUp() {
        systemMock = mock(SystemAbstractionLayer.class);
        this.out = new TestWriter();
     }

    @Test
    public void testNoCommand1RunningReturnsCodeToStartIt4oneApp() {
        new StartupAppsCommand(config1app, systemMock).execute(out);

        assertEquals( 1, out.numLines());
        assertEquals( "#!/bin/bash\n\ncommand1 --param\n", out.line(0));
    }

    @Test
    public void testCommand1RunningReturnsNoCodeToStartIt() {
        systemSetupCommandsRunning("/path/to/command1");

        new StartupAppsCommand(config1app, systemMock).execute(out);

        assertEquals( 1, out.numLines());
        assertEquals( "#!/bin/bash\n\n", out.line(0));
    }

    @Test
    public void testNoCommandsRunningReturnsCodeToStartThem4TwoApp() {
        new StartupAppsCommand(config2apps, systemMock).execute(out);

        assertEquals( 1, out.numLines());
        assertEquals( "#!/bin/bash\n\ncommand1 --param\ncommand2 --param\n", out.line(0));
    }

    @Test
    public void testCommand1RunningReturnsCodeToStartCommand2() {
        systemSetupCommandsRunning("/path/to/command1");

        new StartupAppsCommand(config2apps, systemMock).execute(out);

        assertEquals( 1, out.numLines());
        assertEquals( "#!/bin/bash\n\ncommand2 --param\n", out.line(0));
    }


    @Test
    public void testCommand2RunningReturnsCodeToStartCommand1() {
        systemSetupCommandsRunning("/path/to/command2");

        new StartupAppsCommand(config2apps, systemMock).execute(out);

        assertEquals( 1, out.numLines());
        assertEquals( "#!/bin/bash\n\ncommand1 --param\n", out.line(0));
    }

    @Test
    public void testBothCommandsRunningReturnsCodeToStartNone() {
        systemSetupCommandsRunning("/path/to/command1", "/path/to/command2");

        new StartupAppsCommand(config2apps, systemMock).execute(out);

        assertEquals( 1, out.numLines());
        assertEquals( "#!/bin/bash\n\n", out.line(0));
    }


    private ProcessInfo getProcessInfo(final String command)
    {
        return new ProcessInfo()
        {
            @Override
            public String getCommand()
            {
                return command;
            }
        };
    }

    private void systemSetupCommandsRunning(String... command)
    {
        Collection<ProcessInfo> porcessList = new ArrayList<>(1);
        for (String cmd : command) {
            porcessList.add(getProcessInfo(cmd));
        }
        when(systemMock.processes()).thenReturn( porcessList );
    }
}