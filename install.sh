#!/usr/bin/env bash

thisFolder=`dirname $0`
thisPRG=`basename $0`

installFolder=""
installPort=3000

function showHelp() {


[[ ! -z $1 ]] && \
		echo $1

cat <<EOH

    $thisPRG --folder /path/to/install/folder --port port

EOH

}

function dispatcher() {
	[[ -z $1 ]] && \
		showHelp && exit -1

    while [[ ! -z $1 ]]; do
        local __command=$1
        shift

        case $__command in

            --folder)
                installFolder=$1
                shift
                ;;
            --port)
                installPort=$1
                shift
                ;;
            *)
               showHelp "Invalid switch '${__command}'"
               exit -1
               ;;

        esac
	done
}

function validate() {
    [[ -z $installFolder ]] && \
        showHelp "Invalid install folder" && \
        exit -1
}


function install() {
    cd ${thisFolder} && \
    mkdir -p ${installFolder}/ && \
    mvn package && \
    cp target/bash-startup-1.0.0-SNAPSHOT-jar-with-dependencies.jar ${installFolder} && \
    cat << EOS >${installFolder}/run.sh
#!/usr/bin/env bash

thisFolder=\`dirname \$0\`
port=${installPort}

java \\
     -cp \${thisFolder}/bash-startup-1.0.0-SNAPSHOT-jar-with-dependencies.jar \\
     -Dfv.init.method=guru.bitman.bashstartup.Startup#init \\
     guru.bitman.fictionalvieira.server.Server \${port} &

EOS

    chmod a+x ${installFolder}/run.sh && \
    echo "Install complete!"
}

dispatcher $*
validate
install
